source ~/.bundles.vim


" Set 7 lines to the cursor - when moving vertically using j/k
set so=14

"Always show current position
set ruler
set number
" Set Line Number Color
autocmd ColorScheme * highlight LineNr ctermfg=Gray

set cursorline
autocmd ColorScheme * highlight CursorLine term=underline cterm=underline
" Autocomplete Popup Colors
autocmd ColorScheme * highlight Pmenu ctermfg=White ctermbg=Black
autocmd ColorScheme * highlight PmenuSel ctermfg=Black ctermbg=White

" Ignore case when searching
set ignorecase

" When searching try to be smart about cases 
set smartcase

" Highlight search results
set hlsearch

" Makes search act like search in modern browsers
set incsearch 

" Show matching brackets when text indicator is over them
set showmatch 

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Colors and Fonts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Enable syntax highlighting
syntax enable 
syntax on
" Make vim recognize Prolog files
:autocmd BufNewFile,BufRead *.pl set ft=prolog
try
    colorscheme desert
catch
endtry

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set expandtab
set smarttab
" 1 tab == 4 spaces
set shiftwidth=2
set tabstop=2

set ai "Auto indent
set si "Smart indent
set wrap "Wrap lines
set foldmethod=syntax

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Key mappings for shortcuts
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Map // to search visually selected text
vnoremap // y/<C-R>"<CR>