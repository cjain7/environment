set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'
Plugin 'scrooloose/nerdcommenter'
Plugin 'Valloric/YouCompleteMe'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
"
""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"Configuration for YouCompleteMe
let g:ycm_global_ycm_extra_conf = '~/.vim/ycm_extra_conf.py'
" function for checking if the current file is supported
function! Ycm_supports_file()
  let cur_ft = &filetype
  let supported_ft = 0
  for ft in ['c', 'cpp', 'objc', 'objcpp', 'python', 'cs']
    if ft == cur_ft
      let supported_ft = 1
    endif
  endfor
  return supported_ft
endfunction

" uses the Ycm goto declaration or the vim 'gD', depending
" on whether the current file type is supported by Ycm
function! Ycm_goto_decl()
  if Ycm_supports_file()
    exec "YcmCompleter GoToDefinitionElseDeclaration"
  else
    call feedkeys('gD')
  endif
endfunction
" map the previous function to leader + D
nnoremap <leader>D :call Ycm_goto_decl()<CR>

"End of YCM configuration




