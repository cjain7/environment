#Environment variables
export PS1="\[\e]0;\u@hornet: \W\a\]${debian_chroot:+($debian_chroot)}\u@hornet:\W\$ "


#Aliases
alias jinx='ssh cjain7@jinx-login.cc.gatech.edu'
alias ..='cd ..'
alias upvim='vim +PluginInstall +qall'
alias bliss="cd ~/Documents/GRA/BigData/Code/bliss/"
alias iowavpn="sudo openconnect -s ~/Documents/Misc/vpn-script --no-dtls --authgroup SSLvpn vpn.iastate.edu"
alias discovery="ssh jnk@discovery.its.iastate.edu"
alias tsp="cd /home/chirag/Documents/CSE6140/Project/tsp/tsp"
alias ls='ls --color=auto' 

#Address sanitiser
export ASAN_SYMBOLIZER_PATH=/usr/local/bin/llvm-symbolizer
export ASAN_OPTIONS=symbolize=1
